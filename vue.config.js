const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir); //path.join(__dirname)设置绝对路径
}
module.exports = {
  publicPath: "./", //解决 打包 首页白屏，没资源加载的问题
  outputDir: 'dist',   //build输出目录
  productionSourceMap: false,
  chainWebpack: (config) => {
    config.resolve.alias
      .set("@", resolve("src"))
      .set("com", resolve("src/components"))
      .set("ass", resolve("src/assets"))
      .set("style", resolve("src/style"))
      .set("api", resolve("src/request/api.js"))
    //set第一个参数：设置的别名，第二个参数：设置的路径
  },
  assetsDir: 'assets', //静态资源目录（js, css, img）
  lintOnSave: false, //是否开启eslint
  devServer: {
    disableHostCheck: true,
    port: 80,
    proxy: {  //配置跨域
      '/api': {
        target: 'http://www.zhuosen.top/api',
        changOrigin: true,  //允许跨域
        pathRewrite: {
          '^/api': ''
        }
      },
    }
  }
}

