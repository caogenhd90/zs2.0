/** axios封装
 * 请求拦截、相应拦截、错误统一处理
 */

function getUrlKey(name) {
  return (
    decodeURIComponent(
      (new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(
        location.href
      ) || [, ''])[1].replace(/\+/g, '%20')
    ) || null
  )
}
/** * ajax数据请求接口
    @param {string} type 请求方式 {"get(默认)" | "GET" | "post" | "POST"}
    @param {string} url 请求接口地址
    @param {Object} data 请求时后台所需参数
    @param {bool} async 是否异步(true)或同步(false)请求{true(默认) | false}
    @example: $ajax({
            type: "post",
            url: "",
            data: {}
          }).then(function(ret){

          }).then(function(err){

          });
*/
function ajax({ type, url, data, async }) {
  // 异步对象
  var ajax
  window.XMLHttpRequest
    ? (ajax = new XMLHttpRequest())
    : (ajax = new ActiveXObject('Microsoft.XMLHTTP'))
  !type ? (type = 'get') : (type = type)
  !data ? (data = {}) : (data = data)
  async != false ? (!async ? (async = true) : (async = async)) : ''
  return new Promise(function (resolve, reject) {
    // get 跟post  需要分别写不同的代码
    if (type.toUpperCase() === 'GET') {
      // get请求
      if (data) {
        // 如果有值
        url += '?'
        if (typeof data === 'object') {
          // 如果有值 从send发送
          var convertResult = ''
          for (var c in data) {
            convertResult += c + '=' + data[c] + '&'
          }
          url += convertResult.substring(0, convertResult.length - 1)
        } else {
          url += data
        }
      }
      ajax.open(type, url, async) // 设置 方法 以及 url
      ajax.send(null) // send即可
    } else if (type.toUpperCase() === 'POST') {
      // post请求
      ajax.open(type, url) // post请求 url 是不需要改变
      ajax.setRequestHeader(
        'Content-type',
        'application/x-www-form-urlencoded'
      ) // 需要设置请求报文
      if (data) {
        // 判断data send发送数据
        if (typeof data === 'object') {
          // 如果有值 从send发送
          var convertResult = ''
          for (var c in data) {
            convertResult += c + '=' + data[c] + '&'
          }
          convertResult = convertResult.substring(0, convertResult.length - 1)
          ajax.send(convertResult)
        } else {
          ajax.send(data)
        }
      } else {
        ajax.send() // 木有值 直接发送即可
      }
    }
    // 注册事件
    ajax.onreadystatechange = function () {
      // 在事件中 获取数据 并修改界面显示
      if (ajax.readyState == 4) {
        if (ajax.status === 200) {
          // 返回值： ajax.responseText;
          if (ajax.response && typeof ajax.response !== 'object') {
            resolve(JSON.parse(ajax.response))
          } else {
            resolve(ajax.response)
          }
        } else {
          reject(ajax.status)
        }
      }
    }
  })
}
//获取屏幕高度
var height = document.documentElement.clientHeight || document.body.clientHeight

// 分割数组
function Array_processor(array, subGroupLength) {
  let index = 0
  const newArray = []
  while (index < array.length) {
    newArray.push(array.slice(index, (index += subGroupLength)))
  }
  return newArray
}
// 简单深拷贝数组对象
function replica(obj) {
  return JSON.parse(JSON.stringify(obj))
}
// 深拷贝对象
function clone(obj) {
  if (obj === null) return null
  if (typeof obj !== 'object') return obj
  if (obj.constructor === Date) return new Date(obj)
  var newObj = new obj.constructor() // 保持继承链
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      // 不遍历其原型链上的属性
      var val = obj[key]
      newObj[key] = typeof val === 'object' ? arguments.callee(val) : val // 使用arguments.callee解除与函数名的耦合
    }
  }
  return newObj
}
function Covering(e) {
  return e >= 10 ? e : '0' + e
}
// 判断系统
function device_os() {
  let u = navigator.userAgent
  let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1 // g
  let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) // ios终端
  if (isAndroid) {
    return 'Android'
  } else if (isIOS) {
    return 'IOS'
  } else {
    return 'pc'
  }
}
function senseBrowser() {// 判断浏览器
  var browser = navigator.userAgent.toLowerCase();
  if (browser.match(/Alipay/i) == "alipay") {
    return 'alipay'
  } else if (browser.match(/MicroMessenger/i) == "micromessenger") {
    return 'wx'
  } else {
    console.log("其它浏览器");
  }
}
// 时间搓
function timest(e) {
  var tmp = Date.parse(e).toString();
  tmp = tmp.substr(0, 10);
  return tmp;
}
//获取年月日；
function getYearMonthDay(date) {
  let year = date.getFullYear();
  let month = date.getMonth();
  let day = date.getDate();
  return { year, month, day };
}
// 格式化时间搓
function FomartDate_F(timespan) {
  var dateTime = new Date(+(timespan + '000'))
  var year = dateTime.getFullYear()
  var month = dateTime.getMonth() + 1
  var day = dateTime.getDate()
  var hour = dateTime.getHours()
  var minute = dateTime.getMinutes()
  var second = dateTime.getSeconds()
  var now = new Date()
  var now_new = Date.parse(now) // typescript转换写法
  var milliseconds = 0
  var timeSpanStr

  milliseconds = now_new - (timespan + '000')
  if (milliseconds <= 1000 * 60 * 1) {
    console.log(1)
    timeSpanStr = '刚刚'
  } else if (1000 * 60 * 1 < milliseconds && milliseconds <= 1000 * 60 * 60) {
    console.log(2)

    timeSpanStr = Math.round(milliseconds / (1000 * 60)) + '分钟前'
  } else if (
    1000 * 60 * 60 * 1 < milliseconds &&
    milliseconds <= 1000 * 60 * 60 * 24
  ) {
    console.log(3)

    timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60)) + '小时前'
  } else if (
    1000 * 60 * 60 * 24 < milliseconds &&
    milliseconds <= 1000 * 60 * 60 * 24 * 15
  ) {
    console.log(4)
    timeSpanStr = Math.round(milliseconds / (1000 * 60 * 60 * 24)) + '天前'
  } else if (
    milliseconds > 1000 * 60 * 60 * 24 * 15 &&
    year == now.getFullYear()
  ) {
    console.log(5)
    timeSpanStr =
      Covering(month) +
      '-' +
      Covering(day) +
      ' ' +
      Covering(hour) +
      ':' +
      Covering(minute)
  } else {
    console.log(6)
    timeSpanStr =
      year +
      '-' +
      Covering(month) +
      '-' +
      Covering(day) +
      ' ' +
      Covering(hour) +
      ':' +
      Covering(minute)
  }

  return timeSpanStr
}
// 格式化时间搓
function FomartDate(date, typeData) {
  var type = typeData || 'Y-M-D'
  var date = new Date(date * 1000)
  var y = date.getFullYear()
  var m = Covering(date.getMonth() + 1)
  var d = Covering(date.getDate())
  var h = Covering(date.getHours())
  var f = Covering(date.getMinutes())
  var s = Covering(date.getSeconds())
  type = type.replace('Y', y)
  type = type.replace('M', m)
  type = type.replace('D', d)
  type = type.replace('H', h)
  type = type.replace('F', f)
  type = type.replace('S', s)
  return type
}
// 保留两位有效小数
function round2(e) {
  return Math.round(e * 100) / 100
}
function Validate(form) {

  var v = new RegExp();

  v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");
  if (!v.test(form["URL"].value)) {
    return 'http://' + form;
  }
  return form

}
export {
  FomartDate, // 格式化时间搓
  FomartDate_F, // 格式化时间搓
  timest, // 获取时间搓；
  getUrlKey, // 获取URL参数
  getYearMonthDay, // 获取年月日；
  round2, // 保留两位有效小数
  senseBrowser, // 判断浏览器
  device_os, // 判断系统
  Covering, // 个位数补零
  clone, // 深拷贝对象
  replica, // 简单深拷贝数组对象
  Array_processor, // 分割数组
  height, // 获取屏幕高度
  ajax, // ajax请求promise封装
  Validate, // 判断是否为网址，
}
