import wx from 'weixin-jsapi'
import {fenxiang} from "api";
import {Toast} from "vant";
let option = {
    title: "晫森文学", //分享的标题
    desc:  "晫森文学，晫光为养、载之森壤，以热诚定义精彩原创。",
    link: window.location.href.split("#")[0] ,//分享的路径,
    imgUrl: "http://www.zhuosen.top/uploads/20210108/06b1e6462684dc117eb84ebedf6052ad.png" //分享的图片
};
fenxiang({
    url:option.link
}).then(res => {
    option.link = res.data.url
    wx.config({
        debug: true, // 开启调试模式
        appId: res.data.appId, // 必填，公众号的唯一标识
        timestamp: res.data.timestamp, // 必填，生成签名的时间戳
        nonceStr: res.data.nonceStr, // 必填，生成签名的随机串
        signature: res.data.signature, // 必填，签名，见附录1
        jsApiList: [
            "updateTimelineShareData", //最新的分享朋友圈
            "updateAppMessageShareData", //最新的分享好友
            "onMenuShareAppMessage", //之前的方法分享朋友圈
            "onMenuShareTimeline",//之前的方法分享好友
        ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    })
    wx.ready(function() {
        //分享给朋友
        wx.onMenuShareAppMessage({
            title : option.title, // 分享标题
            desc : option.desc, // 分享描述
            link : option.link, // 分享链接
            imgUrl : option.imgUrl, // 分享图标
            type : 'link', // 分享类型,music、video或link，不填默认为link
            success : function() {
                // 用户确认分享后执行的回调函数
                // alert('成功')
                fenxiang().then(()=>{
                    Toast.success('分享成功！')
                })
            },
            cancel : function() {
                // alert('取消')
                // 用户取消分享后执行的回调函数
            }
        });

    });
});


