import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
// 引入全局重置css
import '@/static/css/reset.css'
import '@/static/css/common.css'
// 引入用户的全局组件
import '@/components/UserGlobalComponents'
// 引入作家的全局组件
import '@/components/WriterGlobalComponents'
// 淘宝的rem
import 'lib-flexible'
// 引入vt组件库
import vt from 'vant'
// 引入vt组件库样式
import 'vant/lib/index.css';
// 引入动画库
import '@/assets/css/animate.min.css'
// 引入下拉刷新
import VueScroller from 'vue-scroller'
Vue.use(VueScroller)
require('es6-promise').polyfill() //加载promise环境
// 注册到Vue中
import { Lazyload } from 'vant';

Vue.use(Lazyload);
import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

import waterfall from 'vue-waterfall2'
Vue.use(waterfall)


Vue.use(VueQuillEditor)
// 注册时可以配置额外的选项
Vue.use(Lazyload, {
    lazyComponent: true,
});
Vue.use(vt)
const res = new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'

var option = {
    maxSpreadZoom: 1, // 控制预览图最大的倍数，默认是2倍，我这里改成了原图
    fullscreenEl: false, //控制是否显示右上角全屏按钮
    closeEl: false, //控制是否显示右上角关闭按钮
    tapToClose: true, //点击滑动区域应关闭图库
    shareEl: false, //控制是否显示分享按钮
    zoomEl: false, //控制是否显示放大缩小按钮
    counterEl: true, //控制是否显示左上角图片数量按钮
    arrowEl: true,  //控制如图的左右箭头（pc浏览器模拟手机时）
    tapToToggleControls: true, //点击应切换控件的可见性
    clickToCloseNonZoomable: true //点击图片应关闭图库，仅当图像小于视口的大小时
}
Vue.use(preview, option)

window.sessionStorage.setItem('isActive', 0)