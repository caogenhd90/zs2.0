import { get, post, get2, post2 } from "./http";
// 首页数据
export const index = (data) => post("/api", data);
//banner
export const banner = (data) => post("/api/index/banner", data);
// 新书上架
export const booknew = (data) => post("/api/index/new", data);
// 排行榜
export const top = (data) => post("/api/index/top", data);
// 猜你喜欢
export const recommended = (data) => post("/api/index/recommended", data);
//限时免费
export const free = (data) => post("/api/index/free", data);
// 租书列表
export const booklist = (data) => post("/api/index/booklist", data);
// 图书详情
export const bookinfo = (data) => post("/api/book/info", data);
// 图书评论列表
export const comment = (data) => post("/api/book/comment", data);
// 图书评论单个详情
export const bookcommentinfo = (data) => post("/api/book/commentinfo", data);
// 评论
export const addcomment = (data) => post("/api/book/addcomment", data);
// 评论点赞
export const addcommentlike = (data) => post("/api/book/addcommentlike", data);
// 打赏
export const reward = (data) => post("/api/book/reward", data);
//章节
export const view = (data) => post("/api/book/view", data);
// 章节订阅
export const subscribe = (data) => post("/api/user/subscribe", data);
//目录
export const mulu = (data) => post("/api/book/mulu", data);
// 图书分类
export const category = (data) => post("/api/book/category", data);
//分类列表
export const categorylist = (data) => post("/api/book/categorylist", data);
//排行榜
export const paihang = (data) => post("/api/book/paihang", data);
//图书搜索
export const search = (data) => post("/api/book/search", data);
// 搜索历史
export const keywords = (data) => post("/api/book/keywords", data);
//用户登录
export const login = (data) => post("/api/user/login", data);
// 书架
export const usershelf = (data) => post("/api/user/usershelf", data);
// 用户信息
export const userinfo = (data) => post("/api/user/info", data);
// 签到
export const sign = (data) => post("/api/user/sign", data);
//删除书架
export const delshelf = (data) => post("/api/user/delshelf", data);
//清空书架
export const alldelshelf = (data) => post("/api/user/alldelshelf", data);
//加入书架
export const addshelf = (data) => post("/api/user/addshelf", data);
//消息
export const usermessage = (data) => post("/api/user/message", data);
//消息
export const userlike = (data) => post("/api/user/like", data);
// 会员中心
export const userindex = (data) => post("/api/user", data);
// 设置
export const userprofile = (data) => post("/api/user/profile", data);
// 金币列表
export const userscore = (data) => post("/api/user/score", data);
// 上传图片
export const upload = (data) => post("/api/upload/image", data);
// 分享
export const usercode = (data) => post("/api/user/code", data);
// 领取任务
export const userlingqu = (data) => post("/api/user/lingqu", data);
// 验证码
export const authcode = (data) => post("/api/user/authcode", data);
// 支付接口
export const apply = (data) => post("/api/user/submit", data);
// 充值记录
export const recharge_order = (data) => post("/api/user/recharge_order", data);
//打赏
export const reward_post = (data) => post("/api/user/reward_post", data);
//推荐票
export const ticket = (data) => post("/api/user/ticket", data);
//金币打赏
export const jinbids = (data) => post("/api/user/jinbi", data);
// 问题反馈
export const feedback = (data) => post("/api/user/feedback", data);
// 关于我们的文字
export const aboutus = (data) => post("/api/index/aboutus", data);
// 社区列表
export const shequcomment = (data) => post("/api/comment/comment", data);
// 社区评论回复
export const create = (data) => post("/api/comment/create", data);
//社区详情
export const commentinfo = (data) => post("/api/comment/info", data);
//点赞
export const commentlike = (data) => post("/api/comment/like", data);
// 福利
export const renwu = (data) => post("/api/user/renwu", data);
//作家消息中心
export const message = (data) => post("/api/author/message", data);
//征文活动
export const solicit = (data) => post("/api/author/solicit", data);
//系统公告
export const systemad = (data) => post("/api/author/systemad", data);
// 点赞列表
export const usercommentlike = (data) => post("/api/user/commentlike", data);
// 评论列表
export const usercomment_post = (data) => post("/api/user/comment_post", data);
//打赏消息
export const rewardMsg = (data) => post("/api/author/reward", data);
//我的邮件
export const email = (data) => post("/api/author/email", data);
//邮件详情
export const adinfo = (data) => post("/api/author/adinfo", data);
//开始创作
export const authorcreate = (data) => get("/api/author/create", data);
// 作品提交
export const createpost = (data) => post("/api/author/createpost", data);
// 默认的章节列表
export const chapterlist = (data) => post("/api/author/chapterlist", data);
// 章节列表
export const volumelist = (data) => post("/api/author/volumelist", data);
// 修改数据
export const volumeupdate = (data) => post("/api/author/volumeupdate", data);
// 创建书卷
export const createvolume = (data) => post("/api/author/createvolume", data);
// 保存章节 
export const createchapter = (data) => post("/api/author/createchapter", data);
// 获取名称
export const new_book_name = (data) => post("/api/author/newbook", data);
//申请签约
export const authorsign = (data) => post("/api/author/sign", data);
// 人物设定
export const authorbookset = (data) => post("/api/author/bookset", data);
// 新增设定
export const booksetcatupdate = (data) => post("/api/author/booksetcatupdate", data);
// 删除小分类
export const booksetcatdel = (data) => post("/api/author/booksetcatdel", data);
// 删除大分类
export const booksetdel = (data) => post("/api/author/booksetdel", data);
// 新建小分类
export const booksetupdate = (data) => post("/api/author/booksetupdate", data);
// 修改图书
export const bookupdate = (data) => post("/api/author/bookupdate", data);
// 开通vip
export const viptype = (data) => post("/api/user/viptype", data);
// 开通VIP
export const vipsubmit = (data) => post("/api/user/vipsubmit", data);
// 租书
export const authorzu = (data) => post("/api/user/authorzu", data);
// 提现类型
export const userwithdrawtype = (data) => post("/api/user/withdrawtype", data);
// 金币提现
export const withdraw = (data) => post("/api/user/withdraw", data);
// 提现记录
export const withdrawlog = (data) => post("/api/user/withdrawlog", data);
// 删除社区评论
export const userdelete = (data) => post("/api/comment/delete", data);
// 分享
export const fenxiang = (data) => post("/api/index/fenxiang", data);
// 分享成功后
export const userfenxiang = (data) => post("/api/user/fenxiang", data);
// 新用户领取福利
export const newuser = (data) => post("/api/user/newuser", data);
// 新社区-热门TAG
export const commentTag = (data) => post("/api/comment/tag", data);
// 标签搜索提示
export const searchTag = (data) => post("/api/comment/tagwhere", data);
//TAG 专区
export const tagall = (data) => post("/api/comment/tagall", data);
//关注专题TAG
export const follow_tag = (data) => post("/api/comment/follow_tag", data);
//收藏
export const collect = (data) => post("/api/comment/collect", data);
//举报列表
export const reportlist = (data) => post("/api/comment/reportlist", data);
// 举报
export const reportapi = (data) => post("/api/comment/report", data);
// 我的关注
export const guanzhu = (data) => post("/api/user/guanzhu", data);
//搜索TAG
// export const follow_tag = (data) => post("/api/comment/follow_tag", data);
// 检查是否重名 
export const checkAuthor = (data) => post2("/api/writer/index/checkAuthor", data);
// 成为作家
export const applyWriter = (data) => post2("/api/writer/index/apply", data);
// 作家个人中心
export const writerCenter = (data) => get2("/api/writer/center/index", data);
// 根据月份获取创作数据接口
export const writercalendar = (data) => get2("/api/writer/center/calendar", data);
// 获取认证信息
export const writerAuth = (data) => get2("/api/writer/center/auth", data);
// 提交认证信息
export const writerPostAuth = (data) => post("/api/writer/center/auth", data);
// 获取协议
export const writerAgreement = (data) => post("/api/writer/index/agreement", data);
// 统计
export const writerstatistics = (data) => get2("/api/writer/statistics/index", data);
// 打赏消息接口
export const writerReward = (data) => get2("/api/writer/message/reward", data);
// 评论消息接口
export const writerComment = (data) => get2("/api/writer/message/comment", data);
// 评论消息接口
export const writersignContract = (data) => get2("/api/writer/index/signContract", data);
// 我的书籍 
export const writermyBook = (data) => get2("/api/writer/index/myBook", data);
// 提交回复
export const writerreplyComment = (data) => post("/api/writer/message/replyComment", data);
// 删除回复
export const writerdelReplyComment = (data) => post2("/api/writer/message/delReplyComment", data);
// 提现页面数据
export const writerwithdraw = (data) => get2("/api/writer/withdraw/index", data);
// 提现
export const writerwithdrawpost = (data) => post2("/api/writer/withdraw/post", data);
// 银行列表
export const writerbanklist = (data) => get2("/api/writer/bank/typeList", data);
// 我的银行卡列表
export const writerbankindex = (data) => get2("/api/writer/bank/index", data);
// 添加银行卡
export const writerbankAdd = (data) => post2("/api/writer/bank/add", data);
// 设置密码
export const writersetPayPasswd = (data) => post2("/api/writer/center/setPayPasswd", data);
// 提现记录
export const writerwithdrawLog = (data) => post2("/api/writer/withdraw/log", data);
// 修改头像
export const writersetting = (data) => post2("/api/writer/center/setting", data);
// 怔文活动
export const writersolicit = (data) => post2("/api/writer/message/solicit", data);
// 作品授权等级
export const writerauthLevel = (data) => get("/api/writer/works/authLevel", data);
// 验证码
export const writersendMessage = (data) => post2("/api/writer/index/sendMessage", data);
// 忘记密码
export const writerforgetPaypasswd = (data) => post2("/api/writer/center/forgetPaypasswd", data);
// 删除作品
export const writerBookdel = (data) => post2("/api/author/bookdel", data);
// 修改分卷
export const volumeupdate2 = (data) => post2("/api/author/volumeupdate", data);
// 删除分卷
export const delvolume = (data) => post2("/api/author/delvolume", data);
// 签约数据
export const signlist = (data) => post2("/api/author/signlist", data);
// 删除章节
export const chapterdel = (data) => post2("/api/author/chapterdel", data);
// 获取章节内容
export const chapterdetails = (data) => post2("api/author/chapter", data);


