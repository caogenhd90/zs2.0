import axios from "axios";
import QS from "qs";
import { Toast } from "vant";
import router from "@/router"; // 引入qs模块，用来序列化post类型的数据，后面会提到

// 公共的请求头
// axios.defaults.baseURL = 'http://192.168.0.201/zuoshen/public/'
axios.defaults.baseURL = 'http://www.zhuosen.top/'
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";
router.beforeEach((to, from, next) => {
	if (to.fullPath) {
		let token = window.localStorage.getItem('token')
		if (token) {
			axios.defaults.headers.token = window.localStorage.getItem('token')
		}
	}
	next()
})

// let debugParameter = '?XDEBUG_SESSION_START=ECLIPSE_DBGP&KEY=16082034555391'
let debugParameter = ''
/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get(url, params) {
	return new Promise((resolve, reject) => {
		axios
			.get(url + debugParameter, {
				params: params,
			})
			.then((res) => {
				resolve(res.data);
			})
			.catch((err) => {
				reject(err.data);
			});
	});
}
/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get2(url, params) {
	return new Promise((resolve, reject) => {
		axios
			.get(url + debugParameter, {
				params: params,
			})
			.then((res) => {
				if (res.data.code == 1) {
					resolve(res.data);
				}
				if (res.data.code == 0) {
					reject(res.data);
				}
			})
			.catch((err) => {
				reject(err.data);
			});
	});
}

/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function post(url, params) {
	return new Promise((resolve, reject) => {
		axios
			.post(url + debugParameter, QS.stringify(params))
			.then((res) => {
				// console.log(res)
				if (res.data.code == 1) {
					resolve(res.data);
				}
				if (res.data.code == 0) {
					console.log(res);
					Toast.fail(res.data.msg);
				}
			})
			.catch((err) => {
				console.log(err);
				reject(err);
			});
	});
}
/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function post2(url, params) {
	return new Promise((resolve, reject) => {
		axios
			.post(url + debugParameter, QS.stringify(params))
			.then((res) => {
				// console.log(res)
				if (res.data.code == 1) {
					resolve(res.data);
				}
				if (res.data.code == 0) {
					reject(res.data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});
}
