const write = [
    {
        path: "/creating",
        component: () => import("@/views/writer/write/creating.vue"),
        meta: { title: "编辑创作" },
    },
    {
        path: "/over",
        component: () => import("@/views/writer/write/over.vue"),
        meta: { title: "创建结果" },
    },
    {
        path: "/edit",
        component: () => import("@/views/writer/write/edit.vue"),
        meta: { title: "正在编辑" },
    },
    {
        path: "/build",
        component: () => import("@/views/writer/write/build.vue"),
        meta: { title: "新建章节" },
    },
    {
        path: "/success",
        component: () => import("@/views/writer/write/success.vue"),
        meta: { title: "发布成功" },
    },
    {
        path: "/contract",
        component: () => import("@/views/writer/write/contract.vue"),
        meta: { title: "申请签约" },
    },
    {
        path: "/WorksSetting",
        component: () => import("@/views/writer/write/WorksSetting.vue"),
        meta: { title: "作品设定" },
    },
    {
        path: "/setup",
        component: () => import("@/views/writer/write/setup.vue"),
        meta: { title: "作品设置" },
    },
    {
        path: "/fenjuan",
        component: () => import("@/views/writer/write/setup/fenjuan.vue"),
        meta: { title: "作品设置" },
    },
    {
        path: "/jianjie",
        component: () => import("@/views/writer/write/setup/jianjie.vue"),
        meta: { title: "作品设置" },
    },
    {
        path: "/name",
        component: () => import("@/views/writer/write/setup/name.vue"),
        meta: { title: "作品设置" },
    },
    {
        path: "/BookSubsectionlist",
        component: () => import("@/views/writer/write/setup/BookSubsectionlist.vue"),
        meta: { title: "分卷设置" },
    },
    {
        path: "/bookSubsection",
        component: () => import("@/views/writer/write/setup/bookSubsection.vue"),
        meta: { title: '' },
    },

]
export default write;