const personal = [
    {
        path: "/certification",
        component: () => import("@/views/writer/personal/certification.vue"),
        meta: { title: "实名认证" },
    },
    {
        path: "/editingMaterials",
        component: () => import("@/views/writer/personal/editingMaterials.vue"),
        meta: { title: "编辑资料" },
    },
    {
        path: "/changePassword",
        component: () => import("@/views/writer/personal/changePassword.vue"),
        meta: { title: "修改密码" },
    },
    {
        path: "/codewordCalendar",
        component: () => import("@/views/writer/personal/codewordCalendar.vue"),
        meta: { title: "码字日历" },
    },
    {
        path: "/Feedback2",
        component: () => import("@/views/writer/personal/Feedback2.vue"),
        meta: { title: "意见反馈" },
    },
    {
        path: "/signContract",
        component: () => import("@/views/writer/personal/signContract.vue"),
        meta: { title: "申请签约" },
    },

]
export default personal;