const writerIndexRouter = [
	{
		path: "/notice",
		name: "notice",
		component: () => import("@/views/writer/message/notice"),
		meta: { title: "系统公告" },
	},
	{
		path: "/rewardMsg",
		name: "rewardMsg",
		component: () => import("@/views/writer/message/rewardMsg"),
		meta: { title: "打赏消息" },
	},
	{
		path: "/commentMsg",
		name: "commentMsg",
		component: () => import("@/views/writer/message/commentMsg"),
		meta: { title: "评论消息" },
	},
	{
		path: "/mail",
		name: "mail",
		component: () => import("@/views/writer/message/mail"),
		meta: { title: "我的邮件" },
	},
	{
		path: "/mailDetail",
		component: () => import("@/views/writer/message/mailDetail"),
		meta: { title: "我的邮件" },
	},
	{
		path: "/essay",
		component: () => import("@/views/writer/message/essay"),
		meta: { title: "征文活动页" },
	},
]
export default writerIndexRouter;