const stat = [
    {
        path: "/writerWithdrawal",
        component: () => import("@/views/writer/stat/writerWithdrawal.vue"),
        meta: { title: "账户提现" },
    },
    {
        path: "/addbank",
        component: () => import("@/views/writer/stat/addbank.vue"),
        meta: { title: "添加银行卡" },
    },
    {
        path: "/setPassword",
        component: () => import("@/views/writer/stat/setPassword.vue"),
        meta: { title: "设置密码" },
    },
    {
        path: "/writerWithdrawalLog",
        component: () => import("@/views/writer/stat/writerWithdrawalLog.vue"),
        meta: { title: "提现记录" },
    },


]
export default stat;