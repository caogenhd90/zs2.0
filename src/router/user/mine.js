const mine = [
    {
        path: "/mine",
        component: () => import("@/views/user/mine/index.vue"),
        meta: { title: "个人中心" },
    },
    {
        path: "/set",
        component: () => import("@/views/user/mine/set.vue"),
        meta: { title: "我的设置" },
    },
    {
        path: "/message",
        component: () => import("@/views/user/mine/message.vue"),
        meta: { title: "我的消息" },
    },
    {
        path: "/invitation",
        component: () => import("@/views/user/mine/invitation.vue"),
        meta: { title: "我的分享" },
    },
    {
        path: "/release",
        component: () => import("@/views/user/mine/release.vue"),
        meta: { title: "我的发布" },
    },
    {
        path: "/follow",
        component: () => import("@/views/user/mine/follow.vue"),
        meta: { title: "我的关注" },
    },
    {
        path: "/feedback",
        component: () => import("@/views/user/mine/feedback.vue"),
        meta: { title: "问题反馈" },
    },
    {
        path: "/writer",
        component: () => import("@/views/user/mine/writer.vue"),
        meta: { title: "问题反馈" },
    },
    {
        path: "/me",
        component: () => import("@/views/user/mine/me.vue"),
        meta: { title: "关于我们" },
    },
    {
        path: "/Recharge",
        component: () => import("@/views/user/mine/Recharge.vue"),
        meta: { title: "充值" },
    },
    {
        path: "/Withdrawal",
        component: () => import("@/views/user/mine/Withdrawal.vue"),
        meta: { title: "提现" },
    },
    // share
    {
        path: "/record",
        component: () => import("@/views/user/mine/record.vue"),
        meta: { title: "提现" },
    },
    {
        path: "/share",
        component: () => import("@/views/user/mine/share.vue"),
        meta: { title: "分享" },
    },
    {
        path: "/Rechargehistory",
        component: () => import("@/views/user/mine/Rechargehistory.vue"),
        meta: { title: "充值记录" },
    },
    {
        path: "/OpenVip",
        component: () => import("@/views/user/mine/OpenVip.vue"),
        meta: { title: "开通会员" },
    },
    {
        path: "/agreement",
        component: () => import("@/views/user/mine/agreement.vue"),
        meta: { title: "会员协议" },
    },
    {
        path: "/Privacy",
        component: () => import("@/views/user/mine/Privacy.vue"),
        meta: { title: "隐私协议" },
    },
    {
        path: "/rules",
        component: () => import("@/views/user/mine/rules.vue"),
        meta: { title: "福利规则" },
    },
    {
        path: "/service",
        component: () => import("@/views/user/mine/service.vue"),
        meta: { title: "用户服务协议" },
    },
]
export default mine;