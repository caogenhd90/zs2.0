const bookshelf = [
    {
        path: "/bookshelf",
        component: () => import("@/views/user/bookshelf/index.vue"),
        meta: { title: "书架首页" },
    },
    {
        path: "/historylist",
        component: () => import("@/views/user/bookshelf/historylist.vue"),
        meta: { title: "历史记录" },
    },
    {
        path: "/management",
        component: () => import("@/views/user/bookshelf/management.vue"),
        meta: { title: "管理书架" },
    },
]
export default bookshelf;