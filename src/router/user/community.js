const community = [
    {
        path: "/community",
        component: () => import("@/views/user/community/home.vue"),
        meta: { title: "灵感社区" },
    },
    {
        path: "/publishbook",
        component: () => import("@/views/user/community/publishbook.vue"),
        meta: { title: "发布帖子" },
    },
    {
        path: "/Tocomment",
        component: () => import("@/views/user/community/Tocomment.vue"),
        meta: { title: "社区" },
    },
    {
        path: "/ling",
        component: () => import("@/views/user/community/home.vue"),
        meta: { title: "新灵感社区" },
    },
    {
        path: "/TAGzone",
        component: () => import("@/views/user/community/TAGzone.vue"),
        meta: { title: "TAG专区" },
    },{
        path: "/TagDetails",
        component: () => import("@/views/user/community/TagDetails.vue"),
        meta: { title: "TAG名" },
    },{
        path: "/searchTAG",
        component: () => import("@/views/user/community/searchTAG.vue"),
        meta: { title: "搜索TAG" },
    },{
        path: "/report",
        component: () => import("@/views/user/community/report.vue"),
        meta: { title: "投诉举报" },
    },{
        path: "/report_",
        component: () => import("@/views/user/community/report_.vue"),
        meta: { title: "投诉举报" },
    },
]
export default community;