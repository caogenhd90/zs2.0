const userIndexRouter = [
    {
        path: "/",
        redirect: "/home",
    },
    {
        path: "/home",
        component: () => import("@/views/user/index/home"),
        meta: { title: "晫光为养、载之森壤，以热诚定义精彩原创。" },
        children: []
    },
    {
        path: "/login",
        component: () => import("@/views/user/login"),
        meta: { title: "test~" },
        children: []
    },
    {
        path: "/Details",
        component: () => import("@/views/user/index/BookDetails.vue"),
        meta: { title: "书籍详情" },
    },
    {
        path: "/chapter",
        component: () => import("@/views/user/index/chapter.vue"),
        meta: { title: "章节阅读" },
    }
    ,
    {
        path: "/search",
        component: () => import("@/views/user/index/search.vue"),
        meta: { title: "章节阅读" },
    }
    ,
    {
        path: "/classification",
        component: () => import("@/views/user/index/classification.vue"),
        meta: { title: "图书分类" },
    }
    ,
    {
        path: "/ranking",
        component: () => import("@/views/user/index/ranking.vue"),
        meta: { title: "图书排行" },
    }
    ,
    {
        path: "/welfare",
        component: () => import("@/views/user/index/welfare.vue"),
        meta: { title: "福利" },
    }
    ,
    {
        path: "/newbook",
        component: () => import("@/views/user/index/newbook.vue"),
        meta: { title: "新书" },
    }
    ,
    {
        path: "/finished",
        component: () => import("@/views/user/index/finished.vue"),
        meta: { title: "完本" },
    }
    ,
    {
        path: "/tuijian",
        component: () => import("@/views/user/index/tuijian.vue"),
        meta: { title: "完本" },
    }
    ,
    {
        path: "/searchlist",
        component: () => import("@/views/user/index/searchlist.vue"),
        meta: { title: "分类详情" },
    }
    ,
    {
        path: "/goodlist",
        component: () => import("@/views/user/index/goodlist.vue"),
        meta: { title: "好书推荐" },
    }
    ,
    {
        path: "/zulist",
        component: () => import("@/views/user/index/zulist.vue"),
        meta: { title: "好书推荐" },
    }
    ,
    {
        path: "/classificationdetails",
        component: () => import("@/views/user/index/classificationdetails.vue"),
        meta: { title: "分类详情" },
    }
    ,
    {
        path: "/bookReview",
        component: () => import("@/views/user/index/bookReview.vue"),
        meta: { title: "书籍评论详情" },
    }
    ,
    {
        path: "/Writebookreviews",
        component: () => import("@/views/user/index/Writebookreviews.vue"),
        meta: { title: "写下你的评论" },
    }
    ,
    {
        path: "/Commentsoncomments",
        component: () => import("@/views/user/index/Commentsoncomments.vue"),
        meta: { title: "评论的评论" },
    }
    ,
    {
        path: "/mulu",
        component: () => import("@/views/user/index/mulu.vue"),
        meta: { title: "章节目录" },
    }

    ,
    {
        path: "/recruit",
        component: () => import("@/views/user/temp/recruit.vue"),
        meta: { title: "活动" },
    }
    // Commentsoncomments
    // mulu
]
export default userIndexRouter