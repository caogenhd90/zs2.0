import Vue from 'vue';
import VueRouter from "vue-router";
import store from '@/store/index.js'
// 书城路由
import userIndexRouter from './user/userIndexRouter'
import bookshelf from './user/bookshelf'
import community from './user/community'
import mine from './user/mine'
//作家路由
import writerIndexRouter from './writer/writerIndexRouter'
import write from './writer/write'
import stat from './writer/stat'
import personal from './writer/personal'
import { isWeixin } from "@/utils";
import { authcode } from "api";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        ...userIndexRouter,
        ...bookshelf,
        ...community,
        ...mine,
        ...writerIndexRouter,
        ...write,
        ...stat,
        ...personal,
        {
            path: "/ApplyWriter",
            component: () => import("@/views/writer/ApplyWriter"),
            meta: { title: "申请成为作家" },
        },
        {
            path: "/writerIndex",
            component: () => import("@/views/writer/index"),
            meta: { title: '' },
            redirect: '/writerIndexRouter',
            children: [
                {
                    path: "/writerIndexRouter",
                    component: () => import("@/views/writer/message/index"),
                    meta: { title: "消息中心" },
                },
                {
                    path: "/write",
                    component: () => import("@/views/writer/write/index.vue"),
                    meta: { title: "创作" },
                },
                {
                    path: "/stat",
                    component: () => import("@/views/writer/stat/index.vue"),
                    meta: { title: "统计" },
                },
                {
                    path: "/personal",
                    component: () => import("@/views/writer/personal/index.vue"),
                    meta: { title: "个人中心" },
                },
            ]
        },
    ]

});


function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

router.beforeEach((to, from, next) => {
    if (to.meta.keepAlive == true) {
        console.log(to.name);
        store.commit('setKeepAlive', to.name)
        return next()
    }
    if (to.name == 'personal' || to.name == 'writerIndexRouter' || to.name == 'write' || to.name == 'stat') {
        store.commit('clearKeepAlive')
        return next()
    }
    window.scrollTo(0, 0)
    if (to.path) {
        // 每次路由执行判断是否有token
        let token = window.localStorage.getItem('token')
        // 没有token进入
        if (!token) {
            if (isWeixin) {
                let code = window.localStorage.getItem('code')
                // 重定向地址重定到当前页面，在路径获取 code
                const hrefUrl = window.location.href
                const redirect_uri = encodeURIComponent(hrefUrl)
                const state = encodeURIComponent(
                    ("" + Math.random()).split(".")[1] + "authorizestate"
                );
                window.localStorage.setItem('code', getQueryString("code"))
                if (code == null) {
                    window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1bfbfe2dfd50f9aa&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`;
                } else { // 存在 code 直接调用接口
                    authcode({
                        code: window.localStorage.getItem('code'),
                    }).then((res) => {
                        console.log(res.data.status)
                        if (res.data.status) {
                            window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1bfbfe2dfd50f9aa&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`;
                        }
                        // window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1bfbfe2dfd50f9aa&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`;
                        window.localStorage.setItem('token', res.data.userinfo.token)
                        window.localStorage.setItem('id', res.data.userinfo.id)
                        window.location.reload();
                    })
                }
            }
        }
    }
    if (to.path == '/bookshelf' || to.path == '/writerIndexRouter') {
        window.localStorage.setItem('syactive', 0)
    }
    if (to.path == '/bookshelf' || to.path == '/writerIndexRouter') {
        window.localStorage.setItem('syactive', 0)
    }
    if (to.path == '/shequ' || to.path == '/write') {
        window.localStorage.setItem('syactive', 1)
    }
    if (to.path == '/community' || to.path == '/stat') {
        window.localStorage.setItem('syactive', 2)
    }
    if (to.path == '/mine' || to.path == '/personal') {
        window.localStorage.setItem('syactive', 3)
    }
    window.document.title = to.meta.title
    next()
})

// const VueRouterPush = router.prototype.push
// router.prototype.push = function push (to) {
//     return VueRouterPush.call(this, to).catch(err => err)
// }
export default router;